<?php

namespace Engine23\ShopperApproved\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_ENABLED          = 'engine23_shopperapproved/general/enabled';
    const XML_PATH_ACCOUNT_ID       = 'engine23_shopperapproved/general/account_id';
    const XML_PATH_DOMAIN           = 'engine23_shopperapproved/general/domain';

    const XML_PATH_SITE_ID          = 'engine23_shopperapproved_section_setup/general/site_id';

    const XML_PATH_ACCOUNT_TOKEN    = 'engine23_shopperapproved/schema/account_token';
    const XML_PATH_MERCHANT_TOKEN   = 'engine23_shopperapproved/schema/merchant_token';

    const XML_PATH_SHOW_EMPTY_STARS = 'engine23_shopperapproved/display/show_empty_stars';

    public function getEnabled()
    {
        return $this->getConfig(self::XML_PATH_ENABLED);
    }

    public function getAccountId()
    {
        return $this->getConfig(self::XML_PATH_ACCOUNT_ID);
    }

    public function getAccountToken()
    {
        return $this->getConfig(self::XML_PATH_ACCOUNT_TOKEN);
    }

    public function getMerchantToken()
    {
        return $this->getConfig(self::XML_PATH_MERCHANT_TOKEN);
    }

    public function getDomain()
    {
        return $this->getConfig(self::XML_PATH_DOMAIN);
    }

    public function showEmptyStars()
    {
        return $this->getConfig(self::XML_PATH_SHOW_EMPTY_STARS);
    }

    public function getConfig($path, $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->getValue($path, $scope);
    }
}