<?php
namespace Engine23\ShopperApproved\Block;

use Magento\Catalog\Block\Product\ReviewRendererInterface;

class ReviewRenderer extends ShopperApproved implements ReviewRendererInterface
{
    protected $_preferredReviewRenderer;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Engine23\ShopperApproved\Helper\Data $helper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Directory\Model\Country $countryDirectory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Catalog\Block\Product\ReviewRendererInterface $preferredReviewRenderer,
        array $data = array()
    )
    {
        parent::__construct($context, $helper, $customerSession, $checkoutSession, $countryDirectory, $registry, $searchCriteriaBuilder, $orderRepository, $data);

        $this->_preferredReviewRenderer = $preferredReviewRenderer;
    }

    public function getReviewsSummaryHtml(\Magento\Catalog\Model\Product $product, $templateType = self::DEFAULT_VIEW, $displayIfNoReviews = false)
    {
        if ($this->getSetting('enabled'))
        {
            //Shopper Approved is enabled, output it
            if ($templateType !== NULL)
                $summaryBlock = $this->getLayout()->createBlock('Engine23\ShopperApproved\Block\Rating\ProductList');
            else $summaryBlock = $this->getLayout()->createBlock('Engine23\ShopperApproved\Block\Rating\Product');

            if ($summaryBlock)
            {
                $summaryBlock->setProduct($product);
                return $summaryBlock->toHtml();
            }
        }
        else
        {
            //Fallback to Magento's DI preferred ReviewRenderer class
            //Make sure that the fallback IS NOT the same as this class...this would become a loop
            $className = get_class($this);
            if (!$this->_preferredReviewRenderer instanceof $className)
                return $this->_preferredReviewRenderer->getReviewsSummaryHtml($product, $templateType, $displayIfNoReviews);
        }

        return '';
    }
}