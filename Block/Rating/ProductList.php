<?php

namespace Engine23\ShopperApproved\Block\Rating;

class ProductList extends \Engine23\ShopperApproved\Block\Rating\Product
{
    protected function _toHtml()
    {
        if ($this->getCurrentProduct())
        {
            if (!$this->getTemplate())
                $this->setTemplate('Engine23_ShopperApproved::engine23/shopperapproved/catalog/product/list/stars.phtml');

            return parent::_toHtml();
        }
        else return '';
    }
}