<?php

namespace Engine23\ShopperApproved\Block\Rating;

class Product extends \Engine23\ShopperApproved\Block\ShopperApproved
{
    protected $_product = NULL;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Engine23\ShopperApproved\Helper\Data $helper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Directory\Model\Country $countryDirectory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $data = array()
    )
    {
        $this->_helper           = $helper;
        $this->_customerSession  = $customerSession;
        $this->_checkoutSession  = $checkoutSession;
        $this->_countryDirectory = $countryDirectory;
        $this->_registry         = $registry;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_orderRepository  = $orderRepository;

        parent::__construct($context, $helper, $customerSession, $checkoutSession, $countryDirectory, $registry, $searchCriteriaBuilder, $orderRepository);

        if (isset($data['product']))
        {
            if ($this->_product instanceof \Magento\Catalog\Model\Product)
                $this->_product = $data['product'];
            else throw new \InvalidArgumentException('Product must be an instance of \Magento\Catalog\Model\Product');
        }
    }

    public function setProduct(\Magento\Catalog\Model\Product $product)
    {
        $this->_product = $product;

        return $this;
    }

    public function getCurrentProduct()
    {
        if (!$this->_product)
            $product = $this->_registry->registry('current_product');
        else $product = $this->_product;

        return $product;
    }

    protected function _toHtml()
    {
        if ($this->getCurrentProduct())
        {
            if (!$this->getTemplate())
                $this->setTemplate('Engine23_ShopperApproved::engine23/shopperapproved/catalog/product/view/stars.phtml');

            return parent::_toHtml();
        }
        else return '';
    }
}