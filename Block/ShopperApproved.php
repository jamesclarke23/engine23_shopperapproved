<?php

namespace Engine23\ShopperApproved\Block;

class ShopperApproved extends \Magento\Framework\View\Element\Template
{
    protected $_helper;
    protected $_customerSession;
    protected $_checkoutSession;
    protected $_countryDirectory;
    protected $_registry;
    protected $_orderRepository;
    protected $_searchCriteriaBuilder;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Engine23\ShopperApproved\Helper\Data $helper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Directory\Model\Country $countryDirectory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $data = array()
    )
    {
        $this->_helper           = $helper;
        $this->_customerSession  = $customerSession;
        $this->_checkoutSession  = $checkoutSession;
        $this->_countryDirectory = $countryDirectory;
        $this->_registry         = $registry;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_orderRepository  = $orderRepository;

        parent::__construct($context, $data);
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    public function getHostName()
    {
        return $this->_helper->getDomain();
    }

    public function getToken()
    {
        return $this->_helper->getAccountToken();
    }

    public function getSiteId()
    {
        return $this->_helper->getAccountId();
    }

    public function showEmptyStars()
    {
        return $this->_helper->showEmptyStars();
    }

    /**
     * Replace spaces in a SKU
     * @param $sku
     * @return string
     */
    public function slugifySku($sku)
    {
        return str_replace(' ', '-', $sku);
    }


    /**
     * Access to Helper configuration
     *
     * @access public
     * @param string $key
     * @return string
     */
    public function getSetting($key = null)
    {
        //Cache settings
        static $data;

        if(empty($data))
        {
            //Initial call, grab all Shopper Approved settings
            $data = array(
                'account' => $this->_helper->getAccountId(),
                'token' => $this->_helper->getAccountToken(),
                'merchant' => $this->_helper->getMerchantToken(),
                'enabled' => $this->_helper->getEnabled()
            );

            //Customer data
            $customer = $this->_customerSession->getCustomer();

            if(!empty($customer))
            {
                $data['name'] = $customer->getName();
                $data['email'] = $customer->getEmail();
                $address = $customer->getDefaultBillingAddress();
                if(!empty($address)) {
                    $address = $customer->getDefaultShippingAddress();
                }

                if(!empty($address))
                {
                    $country = $this->_countryDirectory->loadByCode($address->getCountry());
                    $data['country'] = $country->getName();
                    $data['state'] = $address->getRegion();
                }
            }

            $lastOrderId = $this->_checkoutSession->getLastRealOrderId();

            //Attempt to get order id from checkout success page
            $parentBlock = $this->getParentBlock();

            if($parentBlock)
            {
                $lastOrderId = $parentBlock->getOrderId();
            }

            if (!empty($lastOrderId))
            {
                $data['orderid'] = $lastOrderId;
            }
        }

        return isset($data[$key]) ? $data[$key] : NULL;
    }

    /**
     * Return ShopperApproved URL with http or https
     * @param bool $secure
     * @return string
     */
    public function getShopperApprovedUrl($secure = true)
    {
        $prefix = $secure ? 'https' : 'http';

        return $prefix.'://www.shopperapproved.com';
    }

    /**
     * Allow template access to Magento Registry object
     * @return \Magento\Framework\Registry
     */
    public function getRegistry()
    {
        return $this->_registry;
    }

    /**
     * Load order by its increment id
     * @param $incrementId
     * @return null
     */
    public function loadOrder($incrementId)
    {
        if( $incrementId )
        {
            $searchCriteria = $this->_searchCriteriaBuilder->addFilter('increment_id', $incrementId, 'eq')->create();
            $orderList = $this->_orderRepository->getList($searchCriteria);

            $order = $orderList->getFirstItem();

            return $order;
        }
        else return NULL;
    }
}